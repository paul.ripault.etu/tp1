<img src="images/readme/header-small.jpg" >

# Installations et configuration Linux<!-- omit in toc -->
### (instructions spéciales Université de Lille)<!-- omit in toc -->
- [1. Installation de VSCodium](#1-installation-de-vscodium)
- [2. Installation et configuration de Node](#2-installation-et-configuration-de-node)
- [3. configurer le proxy](#3-configurer-le-proxy)

## 1. Installation de VSCodium
**Pour installer VSCodium sur Linux en mode portable (sans les droits d'admin) :**
- téléchargez la version **"linux x64"** disponible sur https://github.com/VSCodium/vscodium/releases/latest
- décompressez-la dans le dossier de votre choix,
- créez un sous-dossier nommé `"data"` et `"data/tmp"` dans le répertoire de vscodium
	```bash
	cd chemin/vers/vscodium
	mkdir -p data/tmp
	```
- Ajoutez ensuite le sous-dossier `"bin"` de vscodium à votre PATH dans votre fichier `~/.bashrc` :
	```bash
	export PATH=$PATH:chemin/vers/VSCodium/bin
	```
	puis appliquer votre modif dans votre terminal
	```bash
	source ~/.bashrc
	```
- vous pouvez maintenant ouvrir VSCodium via un terminal, avec la commande `codium` :
	```bash
	codium chemin/vers/un/dossier/
	```

## 2. Installation et configuration de Node
**Pour installer Node sans droit d'admin, vous pouvez utiliser [Node Version Manager (nvm)](https://github.com/nvm-sh/nvm).**

- Commencez par ajouter le proxy à votre `~/.bashrc` si ce n'est pas déjà fait :
	```bash
	export http_proxy=http://cache.univ-lille.fr:3128
	export https_proxy=http://cache.univ-lille.fr:3128
	```
	puis
	```bash
	source ~/.bashrc
	```
- Lancez ensuite l'install de `nvm` :
	```bash
	curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
	```
	puis
	```bash
	source ~/.bashrc
	```
- Enfin, téléchargez et installez la version de Node que l'on souhaite (ici la 14) grâce à nvm :
	```bash
	nvm install 14
	```
	> _**NB :** si la commande `nvm` n'est pas reconnue, tentez de fermer et relancer votre terminal. Si ça ne suffit pas, ajoutez les lignes suivantes à votre `.bashrc` :_
	> ```bash
	> export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
	> [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
	> ```
- vérifiez ensuite que la version de node est correcte :
	```bash
	node -v
	```
	doit retourner `v14.15.4`

## 3. configurer le proxy
Il ne reste ensuite plus qu'à indiquer à npm le proxy de l'université. A cause d'un bug pour le moment non résolu, il n'est pas possible d'utiliser le nom de domaine du proxy (cache.univ-lille.fr), il faut à la place utiliser son IP comme ceci :
```bash
npm config set proxy http://193.49.225.25:3128/
```


## Étape suivante <!-- omit in toc -->
Une fois tout installé, vous pouvez revenir aux préparatifs : [A. Préparatifs](A-preparatifs.md#a2-récupération-des-fichiers)